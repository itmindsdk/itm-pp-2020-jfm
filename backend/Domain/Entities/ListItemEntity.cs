using Domain.Common;

namespace Domain.Entities
{
    public class ListItemEntity : AuditableEntity
    {
        public TodoListEntity Parent { get; set; }
        public int Id { get; set; }
        public bool Checked { get; set; }
        public string Title { get; set; }
    }
}