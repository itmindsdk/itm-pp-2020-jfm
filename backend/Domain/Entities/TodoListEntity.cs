using System.Collections;
using System.Collections.Generic;
using Domain.Common;

namespace Domain.Entities
{
    public class TodoListEntity : AuditableEntity
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public IList<ListItemEntity> Items { get; set; }
    }
}