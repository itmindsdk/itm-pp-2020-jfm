namespace Domain.Common
{
    public class TodoListItem
    {
        public int Id { get; set; }
        public bool Checked { get; set; }
        public string Title { get; set; }
    }
}