using System.Collections.Generic;
using Application.Common.Mappings;
using Application.ListItemEntities.Queries.GetListItemEntity;
using AutoMapper;
using Domain.Common;
using Domain.Entities;

namespace Application.ListEntities.Queries.GetListEntity
{
    public class ListEntityDto : IMapFrom<TodoListEntity>
    {
        public int Id { get; set; }
        public string Name { get; set; }
        
        public IList<ListItemEntityDto> Items { get; set; }

        public void Mapping(Profile profile)
        {
            profile.CreateMap<TodoListEntity, ListEntityDto>()
                .ForMember(d => d.Name, opt => opt.MapFrom(s => (string) s.Name));
        }
    }
}