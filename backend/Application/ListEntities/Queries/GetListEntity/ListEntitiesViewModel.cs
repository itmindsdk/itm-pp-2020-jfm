using System.Collections.Generic;

namespace Application.ListEntities.Queries.GetListEntity
{
    public class ListEntitiesViewModel
    {
        public IList<ListEntityDto> ListEntities { get; set; }
    }
}