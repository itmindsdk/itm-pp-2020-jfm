using System.Threading;
using System.Threading.Tasks;
using Application.Common.Interfaces;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace Application.ListEntities.Queries.GetListEntity
{
    public class GetListEntitiesQuery : IRequest<ListEntitiesViewModel>
    {
        public class GetListEntitiesQueryHandler : IRequestHandler<GetListEntitiesQuery, ListEntitiesViewModel>
        {
            private readonly IApplicationDbContext _context;
            private readonly IMapper _mapper;

            public GetListEntitiesQueryHandler(IApplicationDbContext context, IMapper mapper)
            {
                _context = context;
                _mapper = mapper;
            }
            
            public async Task<ListEntitiesViewModel> Handle(GetListEntitiesQuery request, CancellationToken cancellationToken)
            {
                var viewModel = new ListEntitiesViewModel();

                viewModel.ListEntities = await _context.ListEntities
                    .ProjectTo<ListEntityDto>(_mapper.ConfigurationProvider)
                    .ToListAsync(cancellationToken);

                return viewModel;
            }
        }
    }
}