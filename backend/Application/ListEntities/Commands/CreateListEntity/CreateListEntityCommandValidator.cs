using FluentValidation;

namespace Application.ListEntities.Commands.CreateListEntity
{
    public class CreateListEntityCommandValidator : AbstractValidator<CreateListEntityCommand>
    {
        public CreateListEntityCommandValidator()
        {
            RuleFor(listEntityCommand => listEntityCommand.Name)
                .MaximumLength(200)
                .NotEmpty();
        }
    }
}