using System.Collections;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Application.Common.Interfaces;
using Domain.Common;
using Domain.Entities;
using MediatR;

namespace Application.ListEntities.Commands.CreateListEntity
{
    public class CreateListEntityCommand : IRequest<int>
    {
        public string Name { get; set; }
        
        public class CreateListEntityCommandHandler : IRequestHandler<CreateListEntityCommand, int>
        {
            private readonly IApplicationDbContext _context;

            public CreateListEntityCommandHandler(IApplicationDbContext context)
            {
                _context = context;
            }
            
            public async Task<int> Handle(CreateListEntityCommand request, CancellationToken cancellationToken)
            {
                var listEntity = new TodoListEntity
                {
                    Name = request.Name,
                };

                _context.ListEntities.Add(listEntity);

                await _context.SaveChangesAsync(cancellationToken);

                return listEntity.Id;
            }
        }
    }
}