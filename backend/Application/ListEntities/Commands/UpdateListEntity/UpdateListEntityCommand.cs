using System.Threading;
using System.Threading.Tasks;
using Application.Common.Exceptions;
using Application.Common.Interfaces;
using MediatR;

namespace Application.ListEntities.Commands.UpdateListEntity
{
    public class UpdateListEntityCommand : IRequest
    {
        public int Id { get; set; }
        public string Name { get; set; }
        
        public class UpdateListEntityCommandHandler : IRequestHandler<UpdateListEntityCommand>
        {
            private readonly IApplicationDbContext _context;

            public UpdateListEntityCommandHandler(IApplicationDbContext context)
            {
                _context = context;
            }
            
            public async Task<Unit> Handle(UpdateListEntityCommand request, CancellationToken cancellationToken)
            {
                var listEntity = await _context.ListEntities.FindAsync(request.Id);

                if (listEntity == null)
                {
                    throw new NotFoundException(nameof(listEntity), request.Id);
                }

                listEntity.Name = request.Name;

                _context.ListEntities.Update(listEntity);

                await _context.SaveChangesAsync(cancellationToken);
                
                return Unit.Value;
            }
        }
    }
}