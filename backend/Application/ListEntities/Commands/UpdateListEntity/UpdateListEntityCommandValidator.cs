using FluentValidation;

namespace Application.ListEntities.Commands.UpdateListEntity
{
    public class UpdateListEntityCommandValidator : AbstractValidator<UpdateListEntityCommand>
    {
        public UpdateListEntityCommandValidator()
        {
            RuleFor(listEntity => listEntity.Name)
                .MaximumLength(200)
                .NotEmpty();
        }
    }
}