using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Application.Common.Exceptions;
using Application.Common.Interfaces;
using MediatR;

namespace Application.ListEntities.Commands.UpdateListEntity
{
    public class UpdateListItemStatusCommand : IRequest
    {
        public int ListId { get; set; }
        public int ItemId { get; set; }
        public bool Checked { get; set; }
        
        public class UpdateListItemStatusCommandHandler : IRequestHandler<UpdateListItemStatusCommand>
        {
            private readonly IApplicationDbContext _context;

            public UpdateListItemStatusCommandHandler(IApplicationDbContext context)
            {
                _context = context;
            }
            
            public async Task<Unit> Handle(UpdateListItemStatusCommand request, CancellationToken cancellationToken)
            {
                var listEntity = await _context.ListEntities.FindAsync(request.ListId);
                
                if (listEntity == null)
                {
                    throw new NotFoundException(nameof(listEntity), request.ListId);
                }

                var item = listEntity.Items.FirstOrDefault(todoListItem => todoListItem.Id == request.ItemId);

                if (item == null)
                {
                    throw new NotFoundException(nameof(listEntity), request.ListId);
                }

                item.Checked = request.Checked;

                _context.ListEntities.Update(listEntity);

                await _context.SaveChangesAsync(cancellationToken);

                return Unit.Value;
            }
        }
    }
}