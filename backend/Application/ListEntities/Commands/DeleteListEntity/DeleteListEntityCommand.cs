using System.Threading;
using System.Threading.Tasks;
using Application.Common.Exceptions;
using Application.Common.Interfaces;
using Domain.Entities;
using MediatR;

namespace Application.ListEntities.Commands.DeleteListEntity
{
    public class DeleteListEntityCommand : IRequest
    {
        public int Id { get; set; }
        
        public class DeleteListEntityCommandHandler : IRequestHandler<DeleteListEntityCommand>
        {
            private readonly IApplicationDbContext _context;

            public DeleteListEntityCommandHandler(IApplicationDbContext context)
            {
                _context = context;
            }

            public async Task<Unit> Handle(DeleteListEntityCommand request, CancellationToken cancellationToken)
            {
                var listEntity = await _context.ListEntities.FindAsync(request.Id);

                if (listEntity == null)
                {
                    throw new NotFoundException(nameof(TodoListEntity), request.Id);
                }

                _context.ListEntities.Remove(listEntity);

                await _context.SaveChangesAsync(cancellationToken);
                
                return Unit.Value;
            }
        }
    }
}