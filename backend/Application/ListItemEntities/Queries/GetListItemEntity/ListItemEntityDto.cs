using Application.Common.Mappings;
using AutoMapper;
using Domain.Entities;

namespace Application.ListItemEntities.Queries.GetListItemEntity
{
    public class ListItemEntityDto : IMapFrom<ListItemEntity>
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public bool Checked { get; set; }

        public void Mapping(Profile profile)
        {
            profile.CreateMap<ListItemEntity, ListItemEntityDto>()
                .ForMember(d => d.Title, opt => opt.MapFrom(s => (string) s.Title));
        }
    }
}