using System.Threading;
using System.Threading.Tasks;
using Application.Common.Interfaces;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace Application.ListItemEntities.Queries.GetListItemEntity
{
    public class GetListItemEntitiesQuery : IRequest<ListItemEntitiesViewModel>
    {
        public class GetListItemEntitiesQueryHandler : IRequestHandler<GetListItemEntitiesQuery, ListItemEntitiesViewModel>
        {
            private readonly IApplicationDbContext _context;
            private readonly IMapper _mapper;

            public GetListItemEntitiesQueryHandler(IApplicationDbContext context, IMapper mapper)
            {
                _context = context;
                _mapper = mapper;
            }
            public async Task<ListItemEntitiesViewModel> Handle(GetListItemEntitiesQuery request, CancellationToken cancellationToken)
            {
                var viewModel = new ListItemEntitiesViewModel();

                viewModel.ListItemEntities = await _context.ListItemEntities
                    .ProjectTo<ListItemEntityDto>(_mapper.ConfigurationProvider)
                    .ToListAsync(cancellationToken);

                return viewModel;
            }
        }
    }
}