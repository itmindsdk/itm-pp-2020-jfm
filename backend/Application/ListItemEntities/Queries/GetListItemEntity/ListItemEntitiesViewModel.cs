using System.Collections;
using System.Collections.Generic;

namespace Application.ListItemEntities.Queries.GetListItemEntity
{
    public class ListItemEntitiesViewModel
    {
        public IList<ListItemEntityDto> ListItemEntities { get; set; }
    }
}