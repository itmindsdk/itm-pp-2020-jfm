using System.Threading;
using System.Threading.Tasks;
using Application.Common.Exceptions;
using Application.Common.Interfaces;
using Domain.Entities;
using MediatR;

namespace Application.ListItemEntities.Commands.UpdateListItemEntity
{
    public class UpdateListItemCheckedStatusEntityCommand : IRequest
    {
        public int Id { get; set; }
        public bool Checked { get; set; }
        
        public class UpdateListItemCheckedStatusEntityHandler : IRequestHandler<UpdateListItemCheckedStatusEntityCommand>
        {
            private readonly IApplicationDbContext _context;

            public UpdateListItemCheckedStatusEntityHandler(IApplicationDbContext context)
            {
                _context = context;
            }
            
            public async Task<Unit> Handle(UpdateListItemCheckedStatusEntityCommand request, CancellationToken cancellationToken)
            {
                var listItem = await _context.ListItemEntities.FindAsync(request.Id);

                if (listItem == null)
                {
                    throw new NotFoundException(nameof(ListItemEntity), request.Id);
                }

                listItem.Checked = request.Checked;

                _context.ListItemEntities.Update(listItem);

                await _context.SaveChangesAsync(cancellationToken);
                
                return Unit.Value;
            }
        }
    }
}