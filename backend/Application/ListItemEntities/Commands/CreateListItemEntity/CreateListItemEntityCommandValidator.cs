using FluentValidation;

namespace Application.ListItemEntities.Commands.CreateListItemEntity
{
    public class CreateListItemEntityCommandValidator : AbstractValidator<CreateListItemEntityCommand>
    {
        public CreateListItemEntityCommandValidator()
        {
            RuleFor(listItemEntity => listItemEntity.Title)
                .MaximumLength(200)
                .NotEmpty();
        }
    }
}