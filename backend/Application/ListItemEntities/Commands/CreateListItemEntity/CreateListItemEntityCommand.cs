using System.Threading;
using System.Threading.Tasks;
using Application.Common.Exceptions;
using Application.Common.Interfaces;
using Domain.Entities;
using MediatR;

namespace Application.ListItemEntities.Commands.CreateListItemEntity
{
    public class CreateListItemEntityCommand : IRequest<int>
    {
        public string Title { get; set; }
        public int ListId { get; set; }
        
        public class CreateListItemEntityCommandHandler : IRequestHandler<CreateListItemEntityCommand, int>
        {
            private readonly IApplicationDbContext _context;

            public CreateListItemEntityCommandHandler(IApplicationDbContext context)
            {
                _context = context;
            }
            
            public async Task<int> Handle(CreateListItemEntityCommand request, CancellationToken cancellationToken)
            {
                var todoListEntity = await _context.ListEntities.FindAsync(request.ListId);

                if (todoListEntity == null)
                {
                    throw  new NotFoundException(nameof(TodoListEntity), request.ListId);    
                }
                
                var listItemEntity = new ListItemEntity
                {
                    Title = request.Title,
                    Parent = todoListEntity
                };

                _context.ListItemEntities.Add(listItemEntity);

                await _context.SaveChangesAsync(cancellationToken);

                return listItemEntity.Id;
            }
        }
    }
}