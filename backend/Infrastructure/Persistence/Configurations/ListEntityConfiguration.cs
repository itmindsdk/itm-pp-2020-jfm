using Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Infrastructure.Persistence.Configurations
{
    public class ListEntityConfiguration : IEntityTypeConfiguration<TodoListEntity>
    {
        public void Configure(EntityTypeBuilder<TodoListEntity> builder)
        {
            builder.Property(listEntity => listEntity.Name)
                .HasMaxLength(200)
                .IsRequired();
        }
    }
}