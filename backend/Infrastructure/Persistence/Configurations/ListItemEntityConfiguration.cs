using Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Infrastructure.Persistence.Configurations
{
    public class ListItemEntityConfiguration : IEntityTypeConfiguration<ListItemEntity>
    {
        public void Configure(EntityTypeBuilder<ListItemEntity> builder)
        {
            builder.Property(e => e.Title)
                .HasMaxLength(200)
                .IsRequired();

            builder.HasOne<TodoListEntity>(e => e.Parent)
                .WithMany(e => e.Items)
                .IsRequired(false);
        }
    }
}