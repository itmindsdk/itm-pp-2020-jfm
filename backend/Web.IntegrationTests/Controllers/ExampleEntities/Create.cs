﻿
using System.Net;
using System.Threading.Tasks;
using Application.ExampleEntities.Commands.CreateExampleEntity;
using Domain.Enums;
using Shouldly;
using Xunit;

namespace Web.IntegrationTests.Controllers.ExampleEntities
{
    public class Create : IClassFixture<CustomWebApplicationFactory<Startup>>
    {
        private readonly CustomWebApplicationFactory<Startup> _factory;

        public Create(CustomWebApplicationFactory<Startup> factory)
        {
            _factory = factory;
        }

        [Fact]
        public async Task GivenValidCreateExampleEntityCommand_ReturnsSuccessCode()
        {
            var client = _factory.GetAnonymousClient();

            var command = new CreateExampleEntityCommand
            {
                Name = "Test",
                ExampleEnum = ExampleEnum.A
            };

            var content = IntegrationTestHelper.GetRequestContent(command);

            var response = await client.PostAsync($"/api/ExampleEntity", content);

            response.EnsureSuccessStatusCode();
        }

        [Fact]
        public async Task GivenInvalidCreateExampleEntityCommand_ReturnsBadRequest()
        {
            var client = _factory.GetAnonymousClient();

            var command = new CreateExampleEntityCommand
            {
                Name = "This description of this thing will exceed the maximum length. This description of this thing will exceed the maximum length. This description of this thing will exceed the maximum length. This description of this thing will exceed the maximum length.",
                ExampleEnum = ExampleEnum.A
                
            };

            var content = IntegrationTestHelper.GetRequestContent(command);

            var response = await client.PostAsync($"/api/ExampleEntity", content);

            response.StatusCode.ShouldBe(HttpStatusCode.BadRequest);
        }
    }
}
