﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Application.ExampleEntities.Commands.CreateExampleEntity;
using Application.ExampleEntityLists.Commands.CreateExampleEntityList;
using Domain.Enums;
using Shouldly;
using Xunit;

namespace Web.IntegrationTests.Controllers.ExampleEntityLists
{
    public class Create : IClassFixture<CustomWebApplicationFactory<Startup>>
    {
        private readonly CustomWebApplicationFactory<Startup> _factory;

        public Create(CustomWebApplicationFactory<Startup> factory)
        {
            _factory = factory;
        }

        [Fact]
        public async Task GivenValidCreateExampleEntityListCommand_ReturnsSuccessCode()
        {
            var client = _factory.GetAnonymousClient();

            var command = new CreateExampleEntityListCommand
            {
                Name = "Test"
            };

            var content = IntegrationTestHelper.GetRequestContent(command);

            var response = await client.PostAsync($"/api/ExampleEntityList", content);

            response.EnsureSuccessStatusCode();
        }

        [Fact]
        public async Task GivenInvalidCreateExampleEntityListCommand_ReturnsBadRequest()
        {
            var client = _factory.GetAnonymousClient();

            var command = new CreateExampleEntityListCommand
            {
                Name = "This description of this thing will exceed the maximum length. This description of this thing will exceed the maximum length. This description of this thing will exceed the maximum length. This description of this thing will exceed the maximum length.",
            };

            var content = IntegrationTestHelper.GetRequestContent(command);

            var response = await client.PostAsync($"/api/ExampleEntityList", content);

            response.StatusCode.ShouldBe(HttpStatusCode.BadRequest);
        }
    }
}
