﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Application.ExampleEntities.Commands.CreateExampleEntity;
using Application.ExampleEntityLists.Commands.CreateExampleEntityList;
using Domain.Enums;
using Shouldly;
using Xunit;

namespace Application.UnitTests.ExampleEntityLists.Commands.CreateExampleEntityList
{
    public class CreateExampleEntityListCommandTest : CommandTestBase
    {
        [Fact]
        public async Task Handle_ShouldPersistExampleEntityList()
        {
            var command = new CreateExampleEntityListCommand
            {
                Name = "CreateTest"
            };

            var handler = new CreateExampleEntityListCommand.CreateExampleEntityListCommandHandler(Context);

            var result = await handler.Handle(command, CancellationToken.None);

            var entity = Context.ExampleEntityLists.Find(result);

            entity.ShouldNotBeNull();
            entity.Name.ShouldBe(command.Name);
        }

    }
}
