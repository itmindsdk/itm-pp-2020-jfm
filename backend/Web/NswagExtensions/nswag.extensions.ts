﻿export class AuthClient  {
    constructor(private accessToken: string) {
        
    }

    transformHttpRequestOptions(options: RequestInit): Promise<RequestInit> {
        if (options.headers && this.accessToken) {
            (<Headers>options.headers)['Authorization'] = 'Bearer ' + this.accessToken;
            return Promise.resolve(options);
        }
    }
}

export class ClientBase {
    constructor(private authClient: AuthClient) {

    }

    transformOptions(options: RequestInit): Promise<RequestInit> {
        return this.authClient ? this.authClient.transformHttpRequestOptions(options) : Promise.resolve(options);
    }
}