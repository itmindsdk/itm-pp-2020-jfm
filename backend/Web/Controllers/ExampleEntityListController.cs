﻿using System.Threading.Tasks;
using Application.ExampleEntityLists.Commands.CreateExampleEntityList;
using Microsoft.AspNetCore.Mvc;

namespace Web.Controllers
{
    public class ExampleEntityListController : ApiControllerBase
    {
        [HttpPost]
        public async Task<ActionResult<int>> Create(CreateExampleEntityListCommand command)
        {
            return await Mediator.Send(command);
        }
    }
}
