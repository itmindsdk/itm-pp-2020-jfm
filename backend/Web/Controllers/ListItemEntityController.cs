using System.Threading.Tasks;
using Application.ListItemEntities.Commands.CreateListItemEntity;
using Application.ListItemEntities.Commands.UpdateListItemEntity;
using Application.ListItemEntities.Queries.GetListItemEntity;
using Microsoft.AspNetCore.Mvc;

namespace Web.Controllers
{
    public class ListItemEntityController : ApiControllerBase
    {
        [HttpPost("{listId}/listItem")]
        public async Task<ActionResult<int>> CreateListItem(CreateListItemEntityCommand command, int listId)
        {
            return await Mediator.Send(command);
        }

        [HttpGet("{listId}")]
        public async Task<ActionResult<ListItemEntitiesViewModel>> GetItemsForList(int listId)
        {
            return await Mediator.Send(new GetListItemEntitiesQuery());
        }

        [HttpPut("changeStatus/{itemId}")]
        public async Task<ActionResult> UpdateStatus(int itemId, UpdateListItemCheckedStatusEntityCommand command)
        {
            if (itemId != command.Id)
            {
                return BadRequest();
            }
        
            await Mediator.Send(command);
        
            return NoContent();
        }
    }
}