using System.Threading.Tasks;
using Application.ListEntities.Commands.CreateListEntity;
using Application.ListEntities.Commands.DeleteListEntity;
using Application.ListEntities.Commands.UpdateListEntity;
using Application.ListEntities.Queries.GetListEntity;
using Microsoft.AspNetCore.Mvc;

namespace Web.Controllers
{
    public class ListEntityController : ApiControllerBase
    {
        [HttpPost]
        public async Task<ActionResult<int>> CreateList(CreateListEntityCommand command)
        {
            return await Mediator.Send(command);
        }

        [HttpPut("{id}")]
        public async Task<ActionResult> Update(int id, UpdateListEntityCommand command)
        {
            if (id != command.Id)
            {
                return BadRequest();
            }

            await Mediator.Send(command);

            return NoContent();
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult> DeleteList(int id)
        {
            await Mediator.Send(new DeleteListEntityCommand
            {
                Id = id
            });

            return NoContent();
        }

        [HttpGet]
        public async Task<ActionResult<ListEntitiesViewModel>> GetLists()
        {
            return await Mediator.Send(new GetListEntitiesQuery());
        }
    }
}