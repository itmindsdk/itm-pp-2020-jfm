import {NextPage} from "next";
import React, {useCallback, useContext, useEffect, useState} from "react";
import ListAddition from "../components/list_addition";
import {appContext} from "../contexts/AppContext";
import {ListEntityDto} from "../../NSwagTS/backend-api";
import {listClientContext} from "../contexts/ListClientContext";
import {ListReducerActionType} from "../utilities/ListReducer";
import TodoList from "../components/todo_list";

const Index: NextPage = () => {

    const {listEntityClient} = useContext(listClientContext);
    const {todoLists, todoListDispatcher} = useContext(appContext);


    useEffect(() => {
        listEntityClient.getLists().then(
            (response) => {
                todoListDispatcher({
                    type: ListReducerActionType.Reset,
                    data: response.listEntities
                })
            }
        )

    }, [todoListDispatcher, listEntityClient]);

    return (
        <div>
            <ListAddition />

            {
                todoLists.map((listEntityDto: ListEntityDto) => <TodoList list={listEntityDto} key={listEntityDto.id} />)
            }
        </div>
    )
}

export default Index;
