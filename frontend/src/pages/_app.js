import 'bootstrap/dist/css/bootstrap.min.css';
import '../styles/styles.css'
import ListClientContextProvider from "../contexts/ListClientContext";
import AppContextProvider from "../contexts/AppContext";
import ListItemClientContextProvider from "../contexts/ListItemClientContext";

// This default export is required in a new `pages/_app.js` file.
export default function MyApp({ Component, pageProps }) {
    return (
        <ListClientContextProvider>
            <ListItemClientContextProvider>
                <AppContextProvider>
                    <Component {...pageProps} />
                </AppContextProvider>
            </ListItemClientContextProvider>
        </ListClientContextProvider>
    )
}