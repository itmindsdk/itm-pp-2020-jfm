import {AuthClient, ListEntityClient} from "../../NSwagTS/backend-api";
import {createContext} from "react";
import fetch from "isomorphic-unfetch";

type ListClientContext = {
    listEntityClient : ListEntityClient
}

export const listClientContext = createContext<ListClientContext>({listEntityClient: null});

export default function ListClientContextProvider({children}) {
    return (
        <listClientContext.Provider value={{listEntityClient: new ListEntityClient(new AuthClient("list_api_client"), "https://localhost:5001", { fetch })}}>
            {children}
        </listClientContext.Provider>
    )
}