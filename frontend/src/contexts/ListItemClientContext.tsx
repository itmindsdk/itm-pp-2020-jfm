import {AuthClient, ListItemEntityClient} from "../../NSwagTS/backend-api";
import {createContext} from "react";
import fetch from "isomorphic-unfetch";

type ListClientContext = {
    listItemEntityClient : ListItemEntityClient
}

export const listItemClientContext = createContext<ListClientContext>({listItemEntityClient: null});

export default function ListItemClientContextProvider({children}) {
    return (
        <listItemClientContext.Provider value={{listItemEntityClient: new ListItemEntityClient(new AuthClient("list_api_client"), "https://localhost:5001", { fetch })}}>
            {children}
        </listItemClientContext.Provider>
    )
}