import {ListEntityDto, ListItemEntityDto} from "../../NSwagTS/backend-api";
import React, {createContext, Dispatch, useReducer} from "react";
import ListReducer, {AllListActions} from "../utilities/ListReducer";


type AppContext =  {
    todoLists: ListEntityDto[],
    todoListDispatcher: Dispatch<AllListActions<ListEntityDto>>,
    itemsList: ListItemEntityDto[],
    itemListDispatcher: Dispatch<AllListActions<ListItemEntityDto>>
}

export const appContext = createContext<AppContext>(null)

export default function AppContextProvider({children}) {
    const [todoLists, todoListDispatcher] = useReducer(ListReducer<ListEntityDto>("id"), []);
    const [itemsList, itemListDispatcher] = useReducer(ListReducer<ListItemEntityDto>("id"), []);

    return (
        <appContext.Provider value={{todoLists, todoListDispatcher, itemsList, itemListDispatcher}}>
            {children}
        </appContext.Provider>
    )
}