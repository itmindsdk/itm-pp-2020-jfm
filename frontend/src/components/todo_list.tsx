import ListElement from "./list_element";
import {CreateListItemEntityCommand, ListEntityDto, ListItemEntityDto} from "../../NSwagTS/backend-api";
import {FunctionComponent, useCallback, useContext, useReducer, useState} from "react";
import {listItemClientContext} from "../contexts/ListItemClientContext";
import {appContext} from "../contexts/AppContext";
import ListReducer, {ListReducerActionType} from "../utilities/ListReducer";

type Props = {
    list: ListEntityDto
}

const filter = (status) => (item: ListItemEntityDto) => {
    switch (status) {
        case "all": return true;
        case "active": return !item.checked
        case "done": return  item.checked
    }
    return false
}

const TodoList: FunctionComponent<Props> = ({list}) => {

    const [itemTitle, setItemTitle] = useState("")
    const [filterBy, setFilterBy] = useState("all")
    const {listItemEntityClient} = useContext(listItemClientContext);
    const [itemsList, itemListDispatcher] = useReducer(ListReducer<ListItemEntityDto>("id"), list.items);

    const addItemToList = useCallback(
        () => {
            const createListItemEntityCommand = new CreateListItemEntityCommand({
                listId: list.id,
                title: itemTitle
            });

            listItemEntityClient.createListItem(createListItemEntityCommand, list.id)
                .then(itemId => {
                    const listItemEntity = new ListItemEntityDto({title: itemTitle, checked: false, id: itemId})

                    itemListDispatcher({
                        type: ListReducerActionType.Add,
                        data: listItemEntity
                    })

                    setItemTitle("")
            })
        }, [itemTitle, itemListDispatcher, itemsList]
    )

    const numberOfDone = itemsList.reduce((acc, item) => { return acc + (item.checked ? 1 : 0) }, 0)

    return (
        <div className="container">
            <div className="card">
                <div className="card-header">
                    <div className="input-group mb-3">
                        <input type="text" className="form-control" defaultValue={list.name} />
                    </div>

                    <div className="input-group mb-3">
                        <input type="text" className="form-control" placeholder="List item" value={itemTitle} onChange={(e) => setItemTitle(e.target.value)} />
                        <div className="input-group-append">
                            <button className="btn btn-outline-secondary" type="button" onClick={
                                () => addItemToList()
                            }>Add new item to list</button>
                        </div>
                    </div>

                    <div className="btn-group btn-group-toggle switcher" data-toggle="buttons">
                        <label id="all" className="btn btn-secondary" onClick={() => setFilterBy("all")}>All</label>
                        <label id="active" className="btn btn-secondary" onClick={() => setFilterBy("active")}>Active</label>
                        <label id="done" className="btn btn-secondary" onClick={() => setFilterBy("done")}>Done</label>
                    </div>
                </div>

                <div>
                    <ul className="list-group">
                        {
                            itemsList.filter(filter(filterBy)).map((listItem, index) => <ListElement item={listItem} listId={list.id} key={index} />)
                        }
                    </ul>
                </div>

                <div className="card-footer">
                    <p>{numberOfDone}/{itemsList.length} is done</p>
                </div>
            </div>
        </div>
    )
}

export default TodoList