import {FunctionComponent, useCallback, useContext, useState} from "react";
import {listClientContext} from "../contexts/ListClientContext";
import {CreateListEntityCommand, ListEntityDto} from "../../NSwagTS/backend-api";
import {appContext} from "../contexts/AppContext";
import {ListReducerActionType} from "../utilities/ListReducer";

const ListAddition: FunctionComponent = () => {

    const [name, setName] = useState("")
    const {listEntityClient} = useContext(listClientContext);
    const {todoListDispatcher} = useContext(appContext);

    const addTodoList = useCallback(
        () => {
            const createTodoListCommand = new CreateListEntityCommand({name})

            listEntityClient.createList(createTodoListCommand).then(res => {

                todoListDispatcher({
                    type: ListReducerActionType.Add,
                    data: new ListEntityDto({id: res, name, items: []})
                })

                setName("")
            });
        }, [todoListDispatcher, name]
    );

    return (
        <div className="container">
            <h2>Add list</h2>

            <div className="input-group mb-3">
                <input type="text" className="form-control" placeholder="List name" value={name} onChange={ (e) => setName(e.target.value)} />
                <div className="input-group-append">
                    <button className="btn btn-outline-secondary" type="button" id="button-addon2" onClick={
                        () => addTodoList()
                    }>Add new list</button>
                </div>
            </div>
        </div>
    )
}

export default ListAddition