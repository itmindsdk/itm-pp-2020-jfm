import React, {FunctionComponent, useCallback, useContext, useState} from "react";
import {appContext} from "../contexts/AppContext";
import {ListReducerActionType} from "../utilities/ListReducer";
import {ListItemEntityDto, UpdateListItemCheckedStatusEntityCommand} from "../../NSwagTS/backend-api";
import {listItemClientContext} from "../contexts/ListItemClientContext";

type Props = {
    item: ListItemEntityDto
    listId: number
}

const ListElement: FunctionComponent<Props> = (props) => {

    const [checked, setChecked] = useState(props.item.checked)
    const {itemListDispatcher} = useContext(appContext);
    const {listItemEntityClient} = useContext(listItemClientContext);
    const [title, setTitle] = useState(props.item.title)

    const updateTodoListStatus = useCallback(
        () => {
            props.item.checked = !checked;
            setChecked(props.item.checked)

            const updateListItemCheckedStatusEntityCommand = new UpdateListItemCheckedStatusEntityCommand({id: props.item.id, checked: props.item.checked})
            listItemEntityClient.updateStatus(props.item.id, updateListItemCheckedStatusEntityCommand);

            itemListDispatcher({
                type: ListReducerActionType.Update,
                data: props.item
            })

        }, [props.item, checked]
    )

    return(
        <li className="list-group-item">
            <div className="checkbox">
                <label>
                    <input className="test" type="checkbox" checked={checked} onChange={() => updateTodoListStatus()} />{title}
                </label>
            </div>
        </li>
    )
}

export default ListElement;